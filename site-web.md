# Le site web

Actuellement, le site redirige sur le blog Wordpress.

Wordpress a un entête avec les éléments de navigation suivants:

- Actualités (Wordpress-même)
- Tutoriels
- Forums (le forum PhpBB)
- Proposition d'article (un formulaire Wordpress)

Proposition d'Ale:

- Utiliser le même en-tête et la même navigation pour le site, les actualités et le forum.
- Ajouter une revue de presse
- Utiliser un générateur de sites statiques pour les actualités
- Créer une page d'accueil avec les dernières activités dans le blog et dans le forum.
