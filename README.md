# Infrastructure

Infrastructure pour Linuxgraphic.

Ce dépôt contient les informations sur l'infrastructure à disposition de la communauté de <https://linuxgraphics.org>.

- hosting: <https://tuxfamily.org>.
- dns: c'est au nom de Dimitri Robert. (il y a façon pour payer la facture d'OVH sans être la personne de contact).
- mailing list: <https://listengine.tuxfamily.org/linuxgraphic.org/interne/>.
- chat: il y avait `#linuxgraphic` sur freenode...
- le site principal et blog marchent sous wordpress.
- le forum marche sous phpBB.

## Détails

- [Le site the Linuxgraphic](site-web.md)
- [Le forum](forum.md)
