# Le forum

Le forum tourne sous phpBB 3.0. Une mise à jour à phpBB 3.3 est prévue.

## Idées

### Réduire le nombre de forums

Actuellement nous avons:

- Le site Linuxgraphic.org
  - La vie du site
  - Propositions de News
  - Vu dans la Presse
- Section 2D: Questions et Réponses
  - Gimp
  - Inkscape
  - Scribus
  - Divers 2D
- Section 3D: Questions et Réponses
  - Blender
  - POV-Ray et ses outils sous Linux
  - Art of Illusion
  - HeeksCAD
  - Divers 3D
- Section Vidéo: Questions et Réponses
  - Montage vidéo
  - Montage audio
  - Divers
- Interne
  - Forum de test
  - Corbeille
  - Tablettes graphiques
  - ... un tas de vieux forums sans posts depuis plus de cinq ans

Ale suggère une nouvelle structure simplifiée:

- Questions et réponses
  - Édition et retouche d'image: Gimp
  - Peinture numérique: Krita, Gimp et MyPaint
  - Dessin vectoriel: Inkscape
  - Mise en page: Scribus
  - Blender
  - CAD
  - Divers 2D
  - Divers 3D
  - Audio et vidéo
- Communauté
  - La vie du site

### Show the latest posts (and latest changes?)

- does this show the latest posts? <https://www.phpbb.com/customise/db/extension/recent_topics_2/>
  - we still need a way to see the latest changes (posts, signatures)

## Permettre l'édition des posts

Actuellement, il n'est permi de modifier les posts que pendant 60 minutes.

Ceci permet d'éviter que les spammeurs ajoutent leur charge après que leur message ait disparu du radar.

- this extension should show all edits: <https://github.com/primehalo/primepostrevisions>
- <https://www.phpbb.com/customise/db/mod/override_edit_time_limit_and_forum_and_user_limits>
- we should have a moderation plugin that shows:
  - all new posts
  - all new users with email, web, link to messages
  - all new changes in the posts
  - all changes in the signature
  - all changes in the web link in the profile

### Markdown

- add support for markdown: <https://www.phpbb.com/customise/db/extension/markdown/>

## Setting up a local phpBB instance

- ssh into the tuxfamily server.
- use scp to copy the latest backup to your local machine.
- skim through <https://www.phpbb.com/support/docs/en/3.3/kb/article/transferring-your-board-to-a-new-host-or-domain/>.
- once you have your local phpBB you can also read `INSTALL.html#update30_31` (at the time of writing there is no online version of it)
- create a new empty database (`linuxgraphic_forum`; adminer?).
- `mysql -u USERNAME -p --default-character-set=utf8 linuxgraphic_forum < linuxgraphic_forums.mysql.dump` (USERNAME = test).
- get the latest phpBb and unpack it in a place served through apache.
- copy the `config.php`from the server, and adapt it to your local system.

  ```php
      <?php
      // phpBB 3.0.x configuration file adapter from the live one
      $dbms = 'mysql';
      $dbhost = 'localhost';
      $dbport = '';
      $dbname = 'linuxgraphic_forum';
      $dbuser = 'test';
      $dbpasswd = 'test';
      $table_prefix = 'phpbb3_';
      $acm_type = 'file';
      $load_extensions = '';

      @define('PHPBB_INSTALLED', true);
      // @define('DEBUG', true);
      // @define('DEBUG_EXTRA', true);
  ```
- in `.htaccess` remove everything but:

  ```htaccess
      RewriteEngine on

      RewriteBase /forums

      RewriteCond %{REQUEST_FILENAME} !-f
      RewriteCond %{REQUEST_FILENAME} !-d
      RewriteRule ^(.*)$ app.php [QSA,L]
  ```
- launch the `update` script :`http://ww.linuxgraphic.org/forums/install/app.php/update`)
- rename the install directory.
- now the local forum should work.
- download the french language pack
  - <https://www.phpbb.com/support/docs/en/3.3/kb/article/how-to-install-a-language-pack/>
  - copy / move the fr directory from the language and the style directories to the phpBB same directories.

## Create a new theme based on the default prosilver

- read <https://www.phpbb.com/styles/create/>
  - create the `prosilver_linuxgraphic` directory in `styles/`
  - create a `style.cfg`
  - create the skeleton's directories and files.
  - set the relative imports in stylesheet.css
  - activate the style in "personanalisation"
  - each time you change an html file in the template you need to empty the cache in the General tab "Vider la cache"


## Updating PhpBB to 3.3

- replace the title with "Les forums de Linuxgraphic"
- in the db, replace the `smiley_url` for the ones that are missing. the list of emoticones is in "publication > émoticônes" and the UI does not allow to modify the image. the images are in `images/smilies` and we mostly need to add `_e`
- changing the encoding of old posts
  - find the oldest post with the issue (if it can be done this way)
  - find out how to fix the encoding in the db.
  - i see the wrong chars in the dump...
    - we could do the changes in the dump
    - adminer also sees the chars... so it should be possible to write a php script.
      - `Ã©`: é
      - `Ã¨`: è
      - `Ãª`: ê
      - see also `html_sdn.vim`
